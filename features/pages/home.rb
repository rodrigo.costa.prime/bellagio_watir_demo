# frozen_string_literal: true

# :nodoc:
class Home

  def initialize(browser)
    @browser = browser
    @site = 'https://bellagio.mgmresorts.com/en.html'
    @book_room_button = @browser.element(id:'nav-book')
  end

  def start_booking
    @browser.goto(@site)
    @book_room_button.wait_until(&:present?).click
  end

end
