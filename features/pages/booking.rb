# frozen_string_literal: true

# :nodoc:
class Booking

  def initialize(browser)
    @browser = browser
    @start_date = @browser.a(id:"c-14-02-2020")
    @end_date = @browser.a(id:"c-16-02-2020")
    @room_titles = @browser.elements(class:'room-title')
    @booking_buttons = @browser.buttons(class:['btn', 'btn--animate', 'bookroom-cta-section__cta'])
    @booking_buttons2 = @browser.driver.find_elements(class:"pricing-offer-price__button-wrapper")
  end

  def select_dates
    @start_date.wait_until(&:present?).click	
    @end_date.wait_until(&:present?).click 
  end

  def choose_room_by_name(room_name)
    sleep 5
    @room_titles[0].wait_until(&:present?)
    @index = 1
    @room_titles.each_with_index do | room_title, number |
      if room_title.text == room_name
        @index = number
      end
    end

    Watir.default_timeout = 10

    begin
      @browser.element(xpath: "/html/body/div[2]/div[2]/div/div/div/div/div[1]/div[3]/div/div/div/div[3]/div[3]/button").click
    rescue
      a = 1
    end


    begin      
      @booking_buttons[@index].wait_until(&:present?).click
    rescue
      @browser.driver.find_elements(class:"pricing-offer-price__button-wrapper")[0].click
    end

    Watir.default_timeout = 30
  end

end
