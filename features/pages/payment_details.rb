# frozen_string_literal: true

# :nodoc:
class PaymentDetails

	def initialize(browser)
		@browser = browser
		@bill_first_name = @browser.text_field(id:'guest-info-cont-first-name')
		@bill_last_name = @browser.text_field(id:'guest-info-cont-last-name')
		@bill_phone = @browser.text_field(id:'guest-info-cont-phone')
		@bill_email = @browser.text_field(id:'guest-info-login-email-address')
		@bill_name_on_card = @browser.text_field(id:'bill-name-on-card')
		@bill_card_number = @browser.text_field(id:'bill-card-num')
		@bill_bill_card_exp_month = @browser.select_list(id:'bill-card-exp-month')
		@bill_bill_card_exp_year = @browser.select_list(id:'bill-card-exp-year')
		@bill_bill_card_cvv = @browser.text_field(id:'bill-card-cvv')
		@bill_bill_address1 = @browser.text_field(id:'bill-address1')
		@bill_bill_city = @browser.text_field(id:'bill-city')
		@bill_terms = @browser.label(id:'terms-label')
		@bill_submit_button = @browser.button(class:['submit', 'btn-large', 'cta'])
  	end

	def fill_payment_details
    	@bill_first_name.wait_until(&:present?).set('Prime')
    	@bill_last_name.wait_until(&:present?).set('Control Poc')
    	@bill_phone.wait_until(&:present?).set('+5511987654321')
    	@bill_email.wait_until(&:present?).set('poc.prime.control@gmail.com')
    	@bill_name_on_card.wait_until(&:present?).set('Prime Control Poc')
    	@bill_card_number.wait_until(&:present?).set('5371100037140017')
    	@bill_bill_card_exp_month.wait_until(&:present?).select("1")
    	@bill_bill_card_exp_year.wait_until(&:present?).select("2039")
    	@bill_bill_card_cvv.wait_until(&:present?).set('451')
    	@bill_bill_address1.wait_until(&:present?).set('Av Paulista, 1000')
    	@bill_bill_city.wait_until(&:present?).set('Sao Paulo')
    	@bill_terms.wait_until(&:present?).click
    	@bill_submit_button.wait_until(&:present?).click
  	end

  	def assert_message(message)
  		sleep 10
  		@message = @browser.element(class:'error-message')
	    if @message.text != message
	      raise "Returned message content: #{@message.text} *"
	    end
  	end

end