Given("that the user access the booking menu on Bellagio home page") do
  @page.call(Home).start_booking
end

When("select the next weekend") do
  @page.call(Booking).select_dates
end

When("select the room {string}") do |room_name|
  @page.call(Booking).choose_room_by_name(room_name)
end

When("fill the required data to book the room") do
  @page.call(PaymentDetails).fill_payment_details
end

Then("I should see the message {string}") do |message|
  @page.call(PaymentDetails).assert_message(message)
end