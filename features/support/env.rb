require 'cucumber'
require 'faker'
require 'pry'
require 'rb-readline'
require 'rspec'
require 'rspec/expectations'
require 'watir'

#browser = Watir::Browser.new :chrome, headless: true
browser = Watir::Browser.new :chrome

page = lambda{|b, klass| klass.new b}.curry.(browser)

Before do
  @page = page
  @browser = browser
end