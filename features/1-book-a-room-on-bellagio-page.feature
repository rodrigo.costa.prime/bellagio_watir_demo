# language: en
Feature: Book a room on Bellagio Page

    @AUTOMATED @PAW-1 
    Scenario: Book a room for the weekend
        
        Given that the user access the booking menu on Bellagio home page
        When select the next weekend
        And select the room "Penthouse Suite"
        And fill the required data to book the room
        Then I should see the message "Sorry, we failed to process your payment. Please check your credit card information."

